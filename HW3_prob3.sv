typedef enum logic[1:0] {START,TXN,FIN} State;

module hw3_prob3(clk,rst_n,cmd,wrt,SCLK,MOSI,SS_n,done);

    localparam CLK_SCALER = 32;
    localparam CLK_MSB = $clog2(CLK_SCALER)-1;

    input clk;
    input rst_n;
    input wrt;
    input [15:0] cmd;
    output logic SCLK;
    output logic MOSI;
    output logic SS_n;
    output logic done;
    State state;
    State nxt_state;
    logic [CLK_MSB:0] clk_cnt;
    logic [4:0] sclk_cnt;
    logic rst_clk;
    logic [15:0] cmd_buffer;
    logic buffer_cmd;

    always_ff @(posedge clk,negedge rst_n)
        if(rst_n)
            // State transition
            state <= nxt_state;
        else
            // Reset state
            state <= START;

    always_ff @(posedge clk)
        // SM says load input into shift register
        if(buffer_cmd)
            cmd_buffer <= cmd;

    always_ff @(posedge clk, negedge rst_n)
        if(rst_n)
            // Check for sync clk counter reset
            if(rst_clk) begin
                // Reset counters
                clk_cnt <= 1'b0;
                sclk_cnt <= 1'b0;
            end
            else begin
                clk_cnt <= clk_cnt + 1;
                if(clk_cnt == 0) begin
                    sclk_cnt <= sclk_cnt + 1;
                    // Load a shift every sclk period after the first
                    // Should happen encounter 17 times
                    // True for 16 of them
                    if(sclk_cnt > 0)
                        cmd_buffer <= cmd_buffer << 1;
                    // Implicit else, maintain
                end
            end
        else begin
            // Reset counters
            clk_cnt <= 0;
            sclk_cnt <= 0;
        end


    always_comb begin
        // Default values
        done = 1'b0; // Normally low
        SS_n = 1'b1; // Normally high
        SCLK = 1'b0; // Normally low
        MOSI = 1'bz; // Normally Hi-z
        rst_clk = 1'b0; // Normally not reseting
        buffer_cmd = 1'b0; // Normally not loading
        nxt_state = START;
        unique case(state)
            START: 
                // On kickoff
                if(wrt) begin
                    //Transition to TXN
                    nxt_state = TXN;
                    // Reset counters
                    rst_clk = 1'b1;
                    // Assert slave select
                    SS_n = 1'b0;
                    // Load into shift register
                    buffer_cmd = 1'b1;
                end
                else begin
                    // Idle
                    nxt_state = START;
                    rst_clk = 1'b1;
                end
            TXN:
                // In a transaction
                // If packet is not complete, sclk_cnt starts at 1
                if(sclk_cnt < 17) begin
                    // Continue transaction
                    nxt_state = TXN;
                    // Continue asserting slave select
                    SS_n = 1'b0;
                    // Assign SCLK to the MSB of the counter
                    SCLK = clk_cnt[CLK_MSB];
                    // Assign MOSI to MSB of shift register
                    MOSI = cmd_buffer[15];
                end
                else begin
                    // Packet is complete
                    nxt_state = FIN;
                    // Deassert slave select
                    SS_n = 1'b1;
                end
            FIN: begin
                    // Simply assert done for one cycle after slave select and
                    // SCLK have returned to normal
                    nxt_state = START;
                    done = 1'b1;
                end
            default:
                nxt_state = START;
        endcase
    end

endmodule

module hw3_prob3_tb;
    logic clk;
    logic rst_n;
    logic wrt;
    logic [15:0] cmd;

    hw3_prob3 hp3(clk,rst_n,cmd,wrt,SCLK,MOSI,SS_n,done);

    initial begin
        rst_n = 1'b0;
        clk = 1'b0;
        wrt = 1'b0;
        cmd = 16'habcd;
        @(negedge clk);
        rst_n = 1'b1;
        @(negedge clk) wrt = 1'b1;
        @(negedge clk) wrt = 1'b1;
        @(negedge clk) wrt = 1'b0;
    end

    always #1 clk=~clk;

endmodule
