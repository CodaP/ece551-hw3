typedef enum logic[1:0] {START,SHIFT} State;
module hw3_prob4(clk,rst_n,cmd,cmd_rdy,SCLK,MOSI,SS_n);
    input clk;
    input rst_n;
    input SCLK;
    input MOSI;
    input SS_n;
    output logic [15:0] cmd;
    output logic cmd_rdy;

    logic SCLK_1; // First flop in pipeline
    logic SCLK_2; // Second flop in pipeline
    logic SCLK_3; // Third flop in pipeline
    logic MOSI_1;// First flop in pipeline
    logic MOSI_2; // Second flop in pipeline
    logic MOSI_3; // Third flop in pipeline
    logic SS_n_1;// First flop in pipeline
    logic SS_n_2;// Second flop in pipeline
    logic SS_n_3;// Third flop in pipeline

    State state;
    State nxt_state;

    always_ff @(posedge clk,negedge rst_n)
        if(rst_n) begin
            // Transition to the next state
            state <= nxt_state;
            // Push one more down each pipeline
            SCLK_1 <= SCLK;
            SCLK_2 <= SCLK_1;
            SCLK_3 <= SCLK_2;
            MOSI_1 <= MOSI;
            MOSI_2 <= MOSI_1;
            MOSI_3 <= MOSI_2;
            SS_n_1 <= SS_n;
            SS_n_2 <= SS_n_1;
            SS_n_3 <= SS_n_2;
            end
        else begin
            // Reset state and pipeline
            state <= START;
            SCLK_1 <= 1'b0;
            SCLK_2 <= 1'b0;
            SCLK_3 <= 1'b0;
            MOSI_1 <= 1'b0;
            MOSI_2 <= 1'b0;
            MOSI_3 <= 1'b0;
            SS_n_1 <= 1'b1;
            SS_n_2 <= 1'b1;
            SS_n_3 <= 1'b1;
        end


    always_comb begin
        // Default values
        cmd_rdy = 1'b1;
        unique case(state)
            START:
                if(SS_n_3)
                    nxt_state = START;
                else
                    nxt_state = SHIFT;
            SHIFT:
                // Wait for stable SCLK to go high
                begin
                cmd_rdy = 1'b0;
                nxt_state = SHIFT;
                // If slave select is deasserted, packet is finished shifting in, go back to idling
                if(SS_n_2)
                    nxt_state = START;
                // If SCLK negedge detected
                else if(SCLK_3 && ! SCLK_2) begin
                    // Shift in the stable MOSI
                    cmd = cmd << 1;
                    cmd[0] = MOSI_3;
                    end
                end
            default: nxt_state = START;
                
        endcase
    end



endmodule

module hw3_prob4_tb;

    logic clk;
    logic rst_n;
    logic wrt;
    logic [15:0] cmd_in;
    logic [15:0] cmd_out;
    logic cmd_rdy;

    hw3_prob3 hp3(clk,rst_n,cmd_in,wrt,SCLK,MOSI,SS_n,done);
    hw3_prob4 hp4(clk,rst_n,cmd_out,cmd_rdy,SCLK,MOSI,SS_n);

    initial begin
        rst_n = 1'b0;
        clk = 1'b0;
        wrt = 1'b0;
        cmd_in = 16'habcd;
        @(negedge clk);
        rst_n = 1'b1;
        @(negedge clk) wrt = 1'b1;
        @(negedge clk) wrt = 1'b1;
        @(negedge clk) wrt = 1'b0;
        #1200 $stop;
    end

    always #1 clk=~clk;

endmodule
