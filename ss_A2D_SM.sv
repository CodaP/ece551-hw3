typedef enum logic[1:0] {START,RUN_UP,SMP_CMPLT,DONE} State;

module ss_A2D_SM(clk,rst_n, gt, smp_eq_8, strt_cnv,
    clr_dac, inc_dac, clr_smp, inc_smp, accum, cnv_cmplt);

    input clk;
    input rst_n;
    input gt;
    input smp_eq_8;
    input strt_cnv;

    output logic clr_dac;
    output logic inc_dac;
    output logic clr_smp;
    output logic inc_smp;
    output logic accum;
    output logic cnv_cmplt;

    State state;
    State nxt_state;

    always_ff @(posedge clk,rst_n)
        if(rst_n)
            state <= nxt_state;
        else
            state <= START;

    always_comb begin
        nxt_state = START;
        clr_dac = 1'b0;
        clr_smp = 1'b0;
        inc_dac = 1'b0;
        inc_smp = 1'b0;
        cnv_cmplt = 1'b0;
        accum = 1'b0;
        unique case(state)
            START: if(strt_cnv) begin
                       nxt_state = RUN_UP;
                       clr_dac = 1'b1;
                       clr_smp = 1'b1;
                       end
                   else begin
                       clr_dac = 1'b1;
                       clr_smp = 1'b1;
                       nxt_state = START;
                    end
            RUN_UP: if(gt) begin
                    nxt_state = SMP_CMPLT;
                end
                else begin
                    nxt_state = RUN_UP;
                    inc_dac = 1'b1;
                end

            SMP_CMPLT: if(smp_eq_8) begin
                    nxt_state = DONE;
                    accum = 1'b1;
                end
                else begin
                    nxt_state = RUN_UP;
                    inc_smp = 1'b1;
                    accum = 1'b1;
                    clr_dac = 1'b1;
                end
            DONE: begin
                nxt_state = START;
                cnv_cmplt = 1'b1;
                clr_smp = 1'b1;
                clr_dac = 1'b1;
                inc_dac = 1'b1;
            end
            default: nxt_state = START;
        endcase
    end

endmodule
