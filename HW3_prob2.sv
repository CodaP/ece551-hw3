typedef enum logic[1:0] {START,TXN,FIN} State;

module hw3_prob2(clk,rst_n,wrt,SCLK,SS_n,done);

    localparam CLK_SCALER = 32;
    localparam CLK_MSB = $clog2(CLK_SCALER)-1;

    input clk;
    input rst_n;
    input wrt;
    output logic SCLK;
    output logic SS_n;
    output logic done;
    State state;
    State nxt_state;
    logic [CLK_MSB:0] clk_cnt;
    logic [4:0] sclk_cnt;
    logic rst_clk;

    always_ff @(posedge clk,negedge rst_n)
        if(rst_n) begin
            // Flop the state
            state <= nxt_state;
            // Control the SCLK counters
            // Check for sync clk counter reset
            if(rst_clk) begin
                clk_cnt = 1'b0;
                sclk_cnt = 1'b0;
                end
            else begin
                // Normal operation is to increment clk_cnt
                clk_cnt <= clk_cnt + 1;
                // Load a shift every sclk period after the first
                // Should happen encounter 17 times
                // True for 16 of them
                if(clk_cnt == 0)
                    sclk_cnt <= sclk_cnt + 1;
            end
        end
        else begin
            // Reset everything
            clk_cnt <= 0;
            sclk_cnt <= 0;
            state <= START;
        end

    always_comb begin
        // By default we are not done
        done = 1'b0;
        // By default slave is not selected
        SS_n = 1'b1;
        // SCLK is normally low
        SCLK = 1'b0;
        // Do not reset SCLK counters by default
        rst_clk = 1'b0;
        unique case(state)
            START: 
                // Wait until wrt kicks off transaction
                if(wrt) begin
                    // Start transaction
                    nxt_state = TXN;
                    // Reset all counters for SCLK
                    rst_clk = 1'b1;
                    // Assert slave select
                    SS_n = 1'b0;
                end
                else begin
                    // reset all counters for SCLK
                    nxt_state = START;
                    rst_clk = 1'b1;
                end
            TXN:
                // Check if more samples need to be taken
                if(sclk_cnt < 17) begin
                    // Take more samples
                    nxt_state = TXN;
                    SS_n = 1'b0;
                    SCLK = clk_cnt[CLK_MSB];
                end
                else begin
                    // Done taking samples
                    nxt_state = FIN;
                    SS_n = 1'b1;
                end
            FIN: begin
                    // Assert done for one clock
                    nxt_state = START;
                    done = 1'b1;
                end
        endcase
    end

endmodule

module hw3_prob2_tb;
    logic clk;
    logic rst_n;
    logic wrt;

    hw3_prob2 hp2(clk,rst_n,wrt,SCLK,SS_n,done);

    initial begin
        rst_n = 1'b0;
        clk = 1'b0;
        wrt = 1'b0;
        @(negedge clk);
        rst_n = 1'b1;
        @(negedge clk) wrt = 1'b1;
        @(negedge clk) wrt = 1'b1;
        @(negedge clk) wrt = 1'b0;
    end

    always #1 clk=~clk;

endmodule
